dockup: docker-compose.yml
	docker-compose up -d

dockdown: docker-compose.yml
	docker-compose down

php:
	docker exec -it -u dev php bash
